import settings
from flask import Flask, request, redirect, session, url_for, render_template, abort
import monzo
import hashlib
import random

app = Flask(__name__)
app.secret_key = settings.SECRET_KEY


def gen_csrf_token():
    if '_csrf_token' not in session:
        token_str = "".join(
            [random.choice("abcdefghijklmnopqrstuvwxyz1234567890!£$%^&&*()") for i in range(100)]
        )

        token_hash = hashlib.sha256(token_str.encode()).hexdigest()
        session['_csrf_token'] = token_hash
    return session['_csrf_token']


@app.before_request
def csrf_protect():
    if request.method == "POST":
        token = session.pop("_csrf_token", None)
        if not token or token != request.form.get('_csrf_token'):
            abort(403)


def format_date(datetime):
    date = datetime.split("T")[0]

    months = [
    "Jan", "Feb", "March", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
    ]
    year, month, day = date.split("-")

    if day[-1] == "1":
        day_suffix = "st"
    elif day[-1] == "2":
        day_suffix = "nd"
    elif day[-1] == "3":
        day_suffix = "rd"
    else:
        day_suffix = "th"


    formated = f"{day}{day_suffix} {months[int(month)-1]} {year}"

    return formated


def format_money(money):
    div_100 = f"{money / 100}"
    if len(div_100.split(".")[-1]) == 1:
        div_100 += "0"

    return div_100


@app.route("/")
def index():
    if session.get("user_id"):
        accounts = monzo.get_accounts(session.get("access_token"))

        for i, account in enumerate(accounts):
            success, account_info = monzo.get_account_info(session.get("access_token"), account["id"])
            if success:
                accounts[i].update(account_info)
                accounts[i]["owners"] = ", ".join([owner["preferred_name"] for owner in account["owners"]])
                accounts[i]["created"] = format_date(account["created"])
                accounts[i]["balance"] = format_money(account["balance"])
                accounts[i]["spend_today"] = format_money(account["spend_today"])

        return render_template("index.html", accounts=accounts)
    else:
        return redirect(url_for("oauth_login"))


@app.route("/feed/send/", methods=["GET", "POST"])
def create_feed_item_view():
    if request.method == "POST":
        data = request.form
        account_id = data["account_id"]
        url = data["url"]

        payload = {
            "params[title]": data["title"],
            "params[body]": data["body"],
            "params[image_url]": data["image_url"],
            "params[background_color]": f"#{data['background_color']}",
            "params[body_color]": f"#{data['body_color']}",
            "params[title_color]": f"#{data['title_color']}",
        }
        monzo.create_feed_item(session.get("access_token"), account_id, url, payload)

    return redirect(url_for("index"))



@app.route("/oauth/login/")
def oauth_login():
    if session.get("user_id") == None:
        state = monzo.gen_state_key()
        session["state"] = state

        return redirect(
            f"https://auth.monzo.com/?client_id={settings.CLIENT_ID}&redirect_uri={settings.REDIRECT_URI}&response_type=code&state={state}"
        )
    return redirect(url_for("index"))


@app.route("/oauth/callback/")
def oauth_callback():
    code = request.args.get("code")
    state = request.args.get("state")

    if state == session.get("state"):
        success, data = monzo.get_access_token(code)

        session["access_token"] = data["access_token"]
        session["user_id"] = data["user_id"]

    return redirect(url_for("index"))


@app.route("/logout/")
def logout():
    session.clear()
    return redirect(url_for("index"))


app.jinja_env.globals['csrf_token'] = gen_csrf_token


if __name__ == "__main__":
    app.run(host=settings.host, port=settings.port, debug=settings.debug)
