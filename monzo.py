import settings
import hashlib
import random
import requests


def gen_state_key():
    state_random_str = "".join(
        [random.choice("abcdefghijklmnopqrstuvwxyz1234567890!£$%^&&*()") for i in range(100)]
    )

    state_hash = hashlib.sha256(state_random_str.encode()).hexdigest()
    return state_hash


def get_access_token(auth_code):
    payload = {
        "grant_type": "authorization_code",
        "client_id": settings.CLIENT_ID,
        "client_secret": settings.CLIENT_SECRET,
        "redirect_uri": settings.REDIRECT_URI,
        "code": auth_code,
    }
    r = requests.post("https://api.monzo.com/oauth2/token", data=payload, verify=settings.ssl_verify)
    r_json = r.json()

    success = r.status_code != 401

    return success, r_json


def get_accounts(access_token):
    r = requests.get("https://api.monzo.com/accounts", headers={"Authorization": f"Bearer {access_token}"}, verify=settings.ssl_verify)
    r_json = r.json()

    return r_json["accounts"]


def get_account_info(access_token, account_id):
    r = requests.get(
        "https://api.monzo.com/balance",
        headers={"Authorization": f"Bearer {access_token}"},
        params={"account_id": account_id},
        verify=settings.ssl_verify
    )

    return r.status_code == 200, r.json()


def create_feed_item(access_token, account_id, url, params):
    payload = {"account_id": account_id, "type": "basic",  "url": url}
    payload.update(params)

    r = requests.post(
        "https://api.monzo.com/feed",
        headers={"Authorization": f"Bearer {access_token}"},
        data = payload,
        verify=settings.ssl_verify
    )
    r_json = r.json()

    return r_json
