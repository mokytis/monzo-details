import os
from dotenv import load_dotenv
load_dotenv()

CLIENT_ID = os.getenv("client_id")
CLIENT_SECRET = os.getenv("client_secret")

REDIRECT_URI = os.getenv("redirect_uri")

SECRET_KEY = os.getenv("secret_key")


host = "0.0.0.0"
port = 80
debug = os.getenv("debug")

ssl_verify = True
